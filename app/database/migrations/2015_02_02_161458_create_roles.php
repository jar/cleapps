<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles', function($table)
        {
            $table->increments('id');
            $table->string('name');
        });
		
		DB::table('roles')->insert(array(
			'name' => 'Admin'
			));
		DB::table('roles')->insert(array(
			'name' => 'Author'
			));
		DB::table('roles')->insert(array(
			'name' => 'User'
			));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');
	}

}
