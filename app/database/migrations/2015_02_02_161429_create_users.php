<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table){
			$table->increments('id');
			$table->string('name');
			$table->string('surname');
			$table->string('email');
			$table->string('password');
		});
		
		DB::table('users')->insert(array(
			'name' => 'Jarek',
			'surname' => 'Kurzacz',
			'email' => 'test@tmp.pl',
			'password' => Hash::make('test')
			));
			
		DB::table('users')->insert(array(
			'name' => 'Alf',
			'surname' => 'Test',
			'email' => 'alf@tmp.pl',
			'password' => Hash::make('test')
			));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
