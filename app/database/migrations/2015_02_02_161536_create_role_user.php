<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role_user', function($table)
        {
			$table->increments('id');
			$table->integer('role_id');
			$table->integer('user_id');
        });
		
		DB::table('role_user')->insert(array(
			'role_id' => '1',
			'user_id' => '1'
			));
		DB::table('role_user')->insert(array(
			'role_id' => '2',
			'user_id' => '1'
			));
		DB::table('role_user')->insert(array(
			'role_id' => '3',
			'user_id' => '2'
			));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('role_user');
	}

}
