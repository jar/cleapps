<!DOCTYPE html>
<html>
<head>
    <title>Users index</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('users') }}">View All</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a New</a>
    </ul>
</nav>

<h1>All the Users</h1>

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Email</td>
			<td>Role</td>
			<td>Actions</td>
		</tr>
    </thead>
    <tbody>
	@foreach($data as $value)
		<tr>
			<td>{{$value->id}}</td>
			<td>{{$value->name}}</td>
			<td>{{$value->email}}</td>
			
			<td>
				@foreach($value->roles as $role)
					{{$role->name}}
				@endforeach
			</td>
			<td>
				<a class="btn btn-small btn-success" href="{{ URL::to('users/' . $value->id)}}">Show this User</a>
				<a class="btn btn-small btn-info" href="{{ URL::to('users/' . $value->id . '/edit') }}">Edit this User</a>
				<a class="btn btn-small btn-danger delete-user" href="{{ URL::to('users/' . $value->id) }}">Delete this User</a>
			</td>
		</tr>
	@endforeach
    </tbody>
</table>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
	$(document).ready(function(){
		$('.delete-user').click(function(){
			var $this = $(this);
			
			$.ajax({
				type: 'DELETE',
				url: $(this).attr('href'),
				context: document.body
				}).done(function() {
					$this.parents('tr').animate({'opacity':0},1000).hide();
				}).error(function(){
					alert('Error');
				});

			return false;
		});
	})
</script>
</body>
</html>