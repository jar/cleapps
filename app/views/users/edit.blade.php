<!DOCTYPE html>
<html>
<head>
    <title>Users index</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('users') }}">View All</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a New</a>
    </ul>
</nav>

<h1>Create</h1>

{{ Form::model($user,array('route' => array('users.update',$user->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Imię') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
    </div>

	<div class="form-group"> 
        {{ Form::label('surname', 'Nazwisko') }}
        {{ Form::text('surname', Input::old('surname'), array('class' => 'form-control')) }}
    </div>
	
    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('roles', 'Roles') }}
        {{ Form::select('roles[]', array('1' => 'Admin', '2' => 'Author', '3' => 'User'), $selectedRole, array('class' => 'form-control','multiple' => 'multiple')) }}
    </div>

    {{ Form::submit('Create User!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>