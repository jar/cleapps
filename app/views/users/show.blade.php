<!DOCTYPE html>
<html>
<head>
    <title>Users show</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('users') }}">View All</a></li>
        <li><a href="{{ URL::to('users/create') }}">Create a New</a>
    </ul>
</nav>

<h1>Showing {{ $user->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $user->name}} {{$user->surname}}</h2>
        <p>
            <strong>Email:</strong> {{ $user->email }}<br>
            <strong>Role:</strong> 
			@foreach($user->roles as $role)
					{{$role->name}} 
			@endforeach
		</p>
    </div>

</div>
</body>
</html>