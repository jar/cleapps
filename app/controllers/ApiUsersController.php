<?php

class ApiUsersController extends \BaseController {

	public function index()
	{
		return $this->findAll();
	}
	
	public function show($id)
	{
		return $this->findOneById($id);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function findAll()
	{
		return User::with('roles')->get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function findOneById($id)
	{
		return User::with('roles')->where('id',$id)->first();
	}
}
