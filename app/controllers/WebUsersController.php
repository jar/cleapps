<?php

class WebUsersController extends \BaseController {

	protected $user;
	
	public function __construct(ApiUsersController $user){
		$this->user = $user;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('users.index')->with('data',$this->user->findAll());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User;
		$user->name       = Input::get('name');
		$user->email      = Input::get('email');
		$user->surname = Input::get('surname');
		$user->timestamps = false;
		
		$roles = empty(Input::get('roles')) ? NULL : Input::get('roles');
		
		if($user->save()) {
			$user->roles()->attach($roles);
			Session::flash('message', 'Successfully created user!');
			return Redirect::to('users');
		} else {
			Session::flash('message', 'Error created user!');
			return Redirect::to('users');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('users.show')->with('user',$this->user->findOneById($id));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->findOneById($id);
	
		foreach($user->roles as $role) {
				$selectedRole[] = $role->id;
		}
	
		return View::make('users.edit')->with('user',$user)->with('selectedRole',$selectedRole);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);
		$user->name       = Input::get('name');
		$user->email      = Input::get('email');
		$user->surname = Input::get('surname');
		$user->timestamps = false;
		
		$roles = empty(Input::get('roles')) ? NULL : Input::get('roles');
		
		if($user->save()) {
			$user->roles()->sync($roles);
			Session::flash('message', 'Successfully update user!');
			return Redirect::to('users');
		} else {
			Session::flash('message', 'Error update user!');
			return Redirect::to('users');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
	
		if($user->delete()) {
			return Response::json(array('sucess'=>true,'msg'=>'Successfully delete user!'));
		} else {
			return Response::json(array('sucess'=>false,'msg'=>'Error delete user!'));
		}
	}


}
